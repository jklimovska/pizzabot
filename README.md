# pizzabot

Pizzabot is cutting-edge pizza techology research, which is a robot that delivers pizza.
This technology is built with Swift.


### Instructions for the robot could be one of the following:
    N: Move north
    S: Move south
    E: Move east
    W: Move west
    D: Drop pizza

Pizzabot always starts at the origin point, (0,0). As with a Cartesian plane, this point lies at the most south-westerly point of the grid.

For example: if the input is 5x5 (1,3) (4,4) one correct solution would de: ENNNDEEEND. Which means: move east once and north trice then drop pizza, move east trice and north once then drop pizza.

As one more complex example will be:
5x5 (0,0) (1,3) (4,4) (4,2) (4,2) (0,1) (3,2) (2,3) (4,1)
And the solution for this will be: DENNNDEEENDSSDDWWWWSDEEENDWNDEESSD

# Instructions

You can run the application by ```$ .Pizzabot/ swift run pizzabot "[grid size] [coordinates]"```

For example: ```$ .Pizzabot/ swift run pizzabot "5x5 (1,3) (4,4)"```
The above input should give the following output: ENNNDNEEED

For running the tests the following command should be used: ```$ .Pizzabot/ swift test```



