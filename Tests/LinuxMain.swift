import XCTest

import pizzabotTests

var tests = [XCTestCaseEntry]()
tests += pizzabotTests.allTests()
XCTMain(tests)
