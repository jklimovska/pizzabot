import XCTest
import class Foundation.Bundle

final class PizzabotTests: XCTestCase
{
    let pizzabot = Pizzabot()
    let error = PizzabotError()

    override func setUp()
    {
        super.setUp()
    }

    override func tearDown()
    {
        super.tearDown()
    }

    func testPostiveCase()
    {
        XCTAssertEqual(pizzabot.deliverPizza(input: "5x5 (1, 3) (4, 4)"), "ENNNDEEEND")
        XCTAssertEqual(pizzabot.deliverPizza(input: "5x5 (0,0) (1,3) (4,4) (4,2) (4,2) (0,1) (3,2) (2,3) (4,1)"), "DENNNDEEENDSSDDWWWWSDEEENDWNDEESSD")
    }

    func testMissingInput()
    {
        XCTAssertEqual(pizzabot.deliverPizza(input: ""), error.noInput)
    }

    func testNoSize()
    {
        XCTAssertEqual(pizzabot.deliverPizza(input: "(1, 3) (4, 4)"), "ENNNDEEEND")
    }

    func testLetterCoordinates()
    {
        XCTAssertEqual(pizzabot.deliverPizza(input: "5x5 (a, c) (4,4)"), "EEEENNNND")
    }

    func testExtraSpaces()
    {
        XCTAssertEqual(pizzabot.deliverPizza(input: "5 x5 (1 , 3) ( 4, 4)"), "ENNNDEEEND")
    }

    func test3DCoordinate() {
        XCTAssertEqual(pizzabot.deliverPizza(input: "5x5 (1 , 3, 4) ( 4, 4)"), "EEEENNNND")
    }

    func testCoordinatesOutsideBounds()
    {
        XCTAssertEqual(pizzabot.deliverPizza(input: "2x2 (1, 3) (4, 4)"), "")
    }

    func testMissingCoordinateValues()
    {
        XCTAssertEqual(pizzabot.deliverPizza(input: "5x5 ( , ) (4, 4)"), "EEEENNNND")
    }

    func testPerformanceExample()
    {
        self.measure {
        }
    }
}
