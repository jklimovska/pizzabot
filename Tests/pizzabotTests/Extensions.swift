import Foundation

extension String
{
    var numbersOnly: String 
    {
        return self.trimmingCharacters(in: CharacterSet(charactersIn: "0123456789").inverted)
    }
}


extension Coordinates: Equatable 
{
    public static func == (left: Coordinates, right: Coordinates) -> Bool 
    {
        return left.x == right.x && left.y == right.y
    }
}
