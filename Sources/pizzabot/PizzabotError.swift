import Foundation

struct PizzabotError
{
    let noInput = "Please insert more info"
    let noGrid = "No grid size"
    let outsideOfBounds = "Outside of range"
    let incorrectData = "Data in incorrect format"
}
