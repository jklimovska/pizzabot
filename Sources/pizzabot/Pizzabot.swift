import Foundation

class Pizzabot
{
    let error = PizzabotError()
    
    func deliverPizza(input: String)-> String 
    {
        guard input.contains(",") 
        else
        {
            return error.noInput
        }

        let elements = input.replacingOccurrences(of: " ", with: "").components(separatedBy: CharacterSet.init(charactersIn: "\"([{"))
        let gridSize = parseGridSize(elements: elements)
        let coordinates = parseCoordinates(elements: elements)
        let validCoordinates = validateCoordinates(gridSize: gridSize, coordinates: coordinates)
        
        if validCoordinates.count <= 8 
        {
            return calculateAllRoutes(coordinates: validCoordinates, currentPosition: Coordinates(x: 0, y: 0), route: "")
        } 
        else 
        {
            return calculateRoute(coordinates: validCoordinates, currentPosition: Coordinates(x: 0, y: 0), route: "")
        }
    }

    private func parseGridSize(elements: [String]) -> GridSize?
    {
        let gridElement = elements[0].components(separatedBy: CharacterSet.init(charactersIn: "xX"))
        if gridElement.count == 2 
        {
            if let width = Int(gridElement[0].numbersOnly), let height = Int(gridElement[1].numbersOnly) 
            {
                return GridSize(width: width, height: height)
            } 
            else
            {
                print(error.incorrectData)
            }
        }
        else 
        {
            print(error.incorrectData)
        }
        return nil
    }

    private func stringToCoordinates(coordinateString: String) -> Coordinates? 
    {
        let array = coordinateString.components(separatedBy: ",").filter{$0 != ""}
        
        if array.count == 2 
        {
            if let xValue = Int(array[0].numbersOnly), let yValue = Int(array[1].numbersOnly) 
            {
                return Coordinates(x: xValue, y: yValue)
            }
        }
        
        return nil
    }

    private func parseCoordinates(elements: [String])->[Coordinates] 
    {
        let justCoordinates = elements.filter { $0 != elements[0] }

        let setOfCoordinates = justCoordinates.map{
            stringToCoordinates(coordinateString: $0)
        }
        
        return setOfCoordinates.compactMap{$0}
    }

    private func validateCoordinates(gridSize: GridSize?, coordinates: [Coordinates])->[Coordinates]
    {
        if let gridSize = gridSize {
            return coordinates.filter{$0.x<gridSize.height && $0.y<gridSize.width && $0.x >= 0 && $0.y >= 0}
        }
        else
        {
            return coordinates.filter{$0.x > 0 && $0.y > 0}
        }
    }

    private func concatRoute(horizontal: Int, vertical: Int, route: String) -> String 
    {
        let horizontalMoves = String(repeating: (horizontal > 0 ? "E" : "W"), count: abs(horizontal))
        let verticalMoves = String(repeating: (vertical > 0 ? "N" : "S"), count: abs(vertical))
        return route + horizontalMoves + verticalMoves + "D"
    }

    private func calculateRoute(coordinates: [Coordinates], currentPosition: Coordinates, route: String) -> String 
    {
        let coordinate = coordinates[0]

        let horizontalMoves: Int = coordinate.x - currentPosition.x
        let verticalMoves: Int = coordinate.y - currentPosition.y

        let newPositionX = currentPosition.x + horizontalMoves
        let newPositionY = currentPosition.y + verticalMoves
        
        let newRoute = concatRoute(horizontal: horizontalMoves, vertical: verticalMoves, route: route)

        let remainingCoordinates = Array(coordinates.dropFirst())
        
        if remainingCoordinates.count > 0 
        {
            return calculateRoute(coordinates: remainingCoordinates, currentPosition: Coordinates(x: newPositionX, y: newPositionY), route: newRoute)
        }
        
        return newRoute
    }

    private func calculateAllRoutes(coordinates: [Coordinates], currentPosition: Coordinates, route: String)->String 
    {
        var routes: [String] = []
        for coordinate in coordinates
        {
            let horizontalMoves: Int = coordinate.x - currentPosition.x
            let verticalMoves: Int = coordinate.y - currentPosition.y

            let newPositionX = currentPosition.x + horizontalMoves
            let newPositionY = currentPosition.y + verticalMoves

            let newRoute = concatRoute(horizontal: horizontalMoves, vertical: verticalMoves, route: route)

            var newCoordinates = coordinates
            
            if let i = newCoordinates.firstIndex(of: coordinate)
            {
                newCoordinates.remove(at: i)
                routes.append(calculateAllRoutes(coordinates: newCoordinates, currentPosition: Coordinates(x: newPositionX, y: newPositionY), route: newRoute))
            }

            if newCoordinates.count <= 0 {
                return newRoute
            }
        }

        if let shortestRoute = routes.min(by: {$1.count > $0.count}) 
        {
            return shortestRoute
        }
        else 
        {
            return("")
        }
    }
}
