import Foundation

struct Coordinates
{
    public var x: Int
    public var y: Int

    func calculateDistance(from currentCoordinates: Coordinates) -> Int 
    {
        let horizontal = self.x - currentCoordinates.x
        let vertical = self.y - currentCoordinates.y
        return abs(horizontal) + abs(vertical)
    }
}
