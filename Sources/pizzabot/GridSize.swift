import Foundation

public struct GridSize
{
    public var width: Int
    public var height: Int
}
