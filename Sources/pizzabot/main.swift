import Foundation

let pizzabot = Pizzabot()
let error = PizzabotError()

if CommandLine.argc < 2
{
    print(error.noInput)
}
else
{
    let input = CommandLine.arguments[1]
    let output = pizzabot.deliverPizza(input: input)
    
    print(output)
}
